package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.OrderHeader;
import com.xsisacademy.bootcamp.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/orderheader")
public class ApiOrderHeaderController {

	@Autowired
	private OrderHeaderRepository orderHeaderRepository;
	
	@GetMapping
	public ResponseEntity<List<OrderHeader>> showAllData(){
		try {
			List<OrderHeader> orderHeaderData = this.orderHeaderRepository.findAllWhereAmountGreaterThanOne();
			return new ResponseEntity<List<OrderHeader>>(orderHeaderData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<OrderHeader>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("/add")
	public ResponseEntity<Object> saveOrderHeader(@RequestBody OrderHeader orderHeader) {
		String timeDec = String.valueOf(System.currentTimeMillis());
		orderHeader.setReference(timeDec);
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		if (orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<Object>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/last")
	public ResponseEntity<Long> getLastOrder() {
		try {
			Long orderHeaderData = this.orderHeaderRepository.getLastOrderHeader();
			return new ResponseEntity<Long>(orderHeaderData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/total")
	public ResponseEntity<Object> getTotal(@RequestBody OrderHeader orderHeader){
		Long id = orderHeader.getId();
		Optional<OrderHeader> orderHeaderData = this.orderHeaderRepository.findById(id);
		if (orderHeaderData.isPresent()) {
			orderHeader.setId(id);
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<Object>("Save Data Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
