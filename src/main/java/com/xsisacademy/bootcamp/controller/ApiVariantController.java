package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/variant")
public class ApiVariantController {

	@Autowired
	private VariantRepository variantRepository;

	@GetMapping
	public ResponseEntity<List<Variant>> getAllVariant() {
		try {
			List<Variant> variants = this.variantRepository.findByVariant();
			return new ResponseEntity<>(variants, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("/add")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {
		variant.setIsActive(true);
		Variant variantData = this.variantRepository.save(variant);
		if (variantData.equals(variant)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id) {
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("/getvariantbycategory/{id}")
	public ResponseEntity<List<Variant>> getVariantByCategoryId(@PathVariable("id") Long id) {
		try {
			List<Variant> variants = this.variantRepository.findVariantByCategoryId(id);
			return new ResponseEntity<>(variants, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("/edit/{id}")
	public ResponseEntity<Object> updateVariant(@RequestBody Variant variant, @PathVariable("id") Long id) {
		Optional<Variant> variantData = this.variantRepository.findById(id);
		variant.setIsActive(true);
		if (variantData.isPresent()) {
			variant.setId(id);
			this.variantRepository.save(variant);
			ResponseEntity rest = new ResponseEntity<>("Update Successfully", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("/delete/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			Variant variant = new Variant();
			variant.setId(id);
			this.variantRepository.deleteVariantById(id);
			ResponseEntity rest = new ResponseEntity<>("Delete Successfully", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/search")
	public ResponseEntity<List<Variant>> searchVariant(@Param("keyword") String keyword) {
		if (keyword != null) {
			List<Variant> variantData = this.variantRepository.searchVariant(keyword);
			return new ResponseEntity<>(variantData, HttpStatus.OK);
		} else {
			List<Variant> variants = this.variantRepository.findByVariant();
			return new ResponseEntity<>(variants, HttpStatus.OK);
		}
	}

}
