package com.xsisacademy.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.OrderDetail;
import com.xsisacademy.bootcamp.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/orderdetail")
public class ApiOrderDetailController {

	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@GetMapping("/getorderbyheaderid/{id}")
	public ResponseEntity<List<OrderDetail>> getOrderDetailById(@PathVariable("id") Long id) {
		try {
			List<OrderDetail> orderDetailData = this.orderDetailRepository.findByHeaderId(id);
			return new ResponseEntity<List<OrderDetail>>(orderDetailData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<OrderDetail>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail) {
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		if (orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<Object>("Data Saved Succesfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Data Failed", HttpStatus.NO_CONTENT);
		}
	}

}
