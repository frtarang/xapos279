package com.xsisacademy.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class home {

	@GetMapping("index")
	public String index() {
		return "index";
	}
	
	@GetMapping("indexexample")
	public String indexexample() {
		return "indexexample";
	}

	@GetMapping("form")
	public String form() {
		return "form";
	}

	@GetMapping("calculator1")
	public String calculator1() {
		return "calculator1";
	}
	
	@GetMapping("calculator2")
	public String calculator2() {
		return "calculator2";
	}

}
