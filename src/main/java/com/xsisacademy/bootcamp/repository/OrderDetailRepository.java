package com.xsisacademy.bootcamp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>{

	@Query("FROM OrderDetail WHERE headerId = ?1")
	List<OrderDetail> findByHeaderId(Long id);

}
