package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("FROM Product ORDER BY productCode")
	public List<Product> findByProduct();

	@Modifying
	@Query(value = "UPDATE Product p SET is_active = false WHERE p.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductById(Long id);

	@Query("FROM Product WHERE LOWER(productCode) LIKE LOWER(concat('%',?1,'%')) OR LOWER(productName) LIKE LOWER(concat('%',?1,'%')) ORDER BY productCode")
	public List<Product> searchProduct(String keyword);

}
